package com.datayes.taohf.lifefun.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.common.App;

/**
 * $DESC$
 * Created by hongfei.tao on 2016/8/6.
 */
public class ViewPagerIndicator extends LinearLayout implements ViewPager.OnPageChangeListener {

    private int mDotCount = 0;

    private int mSelectedResId;

    private int mNormalResId;

    private OnDotChangedListener mOnDotChangedListener;

    private static final int DEFAULT_SELECTED_RESID = R.drawable.shape_dot_selected;

    private static final int DEFAULT_NORMAL_RESID = R.drawable.shape_dot_normal;

    public ViewPagerIndicator(Context context) {
        this(context, null, 0);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mSelectedResId = DEFAULT_SELECTED_RESID;
        mNormalResId = DEFAULT_NORMAL_RESID;
        mDotCount = 0;
    }

    /**
     * 设置指示器点的选中和非选中状态的图片
     *
     * @author hongfei.tao on 2016/8/6 18:06
     */
    public void setResId(int selectedResId, int normalResId) {
        mSelectedResId = selectedResId;
        mNormalResId = normalResId;
    }

    /**
     * 将viewPager跟indicator绑定
     *
     * @param viewPager 传入绑定的viewpager
     * @author hongfei.tao on 2016/8/6 17:41
     */
    public void setupWithViewPager(ViewPager viewPager) {
        if (viewPager != null) {
            mDotCount = viewPager.getAdapter().getCount();
            viewPager.addOnPageChangeListener(this);
            initView();
        }
    }

    public void setOnDotChangedListener(OnDotChangedListener listener) {
        mOnDotChangedListener = listener;
    }

    private void initView() {
        for (int i = 0; i < mDotCount; i++) {
            View view = new View(getContext());
            int width = App.getInstance().dip2px(5);
            int height = width;
            LayoutParams layoutParams = new LayoutParams(width, height);

            if (i != 0) {
                layoutParams.leftMargin = App.getInstance().dip2px(10);
                view.setBackgroundResource(mNormalResId);
            } else {
                view.setBackgroundResource(mSelectedResId);
            }
            view.setLayoutParams(layoutParams);

            addView(view);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (mOnDotChangedListener != null) {
            mOnDotChangedListener.onDotChanged(position);
        }

        for (int i = 0; i < mDotCount; i++) {
            View view = getChildAt(i);
            if (i == position) {
                view.setBackgroundResource(mSelectedResId);
            } else {
                view.setBackgroundResource(mNormalResId);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public interface OnDotChangedListener {
        void onDotChanged(int position);
    }
}
