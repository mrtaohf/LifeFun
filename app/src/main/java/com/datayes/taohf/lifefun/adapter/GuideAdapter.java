package com.datayes.taohf.lifefun.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.activity.GuideActivity;

import java.util.List;

/**
 * 引导页面的adapter
 * Created by hongfei.tao on 2016/8/6.
 */
public class GuideAdapter extends PagerAdapter implements View.OnClickListener {

    private List<String> mList;

    private CallBack mHost;

    public GuideAdapter(GuideActivity guideActivity, List<String> list) {
        mHost = guideActivity;

        mList = list;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        View view = View.inflate(container.getContext(), R.layout.item_guide, null);
        TextView tvDesc = (TextView) view.findViewById(R.id.tv_guide_desc);
        TextView tvEnter = (TextView) view.findViewById(R.id.btn_enter_now);
        tvEnter.setOnClickListener(this);
        tvDesc.setText(mList.get(position));
        if (position == mList.size() - 1) {
            tvEnter.setVisibility(View.VISIBLE);
        } else {
            tvEnter.setVisibility(View.GONE);
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void onClick(View v) {
        mHost.onBtnClick();
    }

    public interface CallBack {
        void onBtnClick();
    }
}
