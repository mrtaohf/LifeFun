package com.datayes.taohf.lifefun.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.activity.DetailActivity;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.model.bean.WeixinBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 微信精选页面适配器
 * Created by hongfei.tao on 2016/8/14.
 */
public class WeixinAdapter extends RecyclerView.Adapter<WeixinAdapter.WeixinViewHolder> {

    private Context mContext;
    private List<WeixinBean.ResultBean.ListBean> mList;

    public WeixinAdapter(Context context) {
        mContext = context;
        if (mList == null) {
            mList = new ArrayList<>();
        }
    }

    public void setList(List<WeixinBean.ResultBean.ListBean> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public WeixinViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeixinViewHolder(View.inflate(parent.getContext(), R.layout.item_weixin, null));
    }

    @Override
    public void onBindViewHolder(WeixinViewHolder holder, int position) {
        final WeixinBean.ResultBean.ListBean listBean = mList.get(position);

        if (listBean != null) {
            holder.mTitle.setText(listBean.getTitle());
            App.getInstance().simpleDisplayImage(listBean.getFirstImg(), holder.mIcon);
            holder.mSource.setText(listBean.getSource());

            holder.mContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DetailActivity.class);
                    intent.putExtra(Constant.KEY_DETAIL_URL, listBean.getUrl());
                    intent.putExtra(Constant.KEY_DETAIL_TITLE, listBean.getTitle());
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class WeixinViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_weixin_container)
        RelativeLayout mContainer;
        @BindView(R.id.iv_icon_weixin)
        ImageView mIcon;
        @BindView(R.id.tv_title_weixin)
        TextView mTitle;
        @BindView(R.id.tv_source_weixin)
        TextView mSource;

        public WeixinViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
