package com.datayes.taohf.lifefun.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.model.bean.JokeBean;

import java.util.List;

import butterknife.ButterKnife;

/**
 * 笑话段子数据bean
 * Created by hongfei.tao on 2016/8/14.
 */
public class JokeAdapter extends RecyclerView.Adapter<JokeAdapter.JokeViewHolder> {

    private Context mContext;
    private List<JokeBean.ResultBean.DataBean> mList;

    public JokeAdapter(Context context, List<JokeBean.ResultBean.DataBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setList(List<JokeBean.ResultBean.DataBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    @Override
    public JokeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new JokeViewHolder(View.inflate(mContext, R.layout.item_joke, null));
    }

    @Override
    public void onBindViewHolder(JokeViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    static class JokeViewHolder extends RecyclerView.ViewHolder {

        public JokeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
