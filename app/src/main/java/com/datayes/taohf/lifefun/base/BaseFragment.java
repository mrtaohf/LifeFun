package com.datayes.taohf.lifefun.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by hongfei.tao on 2016/8/3.
 */
public abstract class BaseFragment extends Fragment implements BaseViewInterface {

    protected Context mContext;
    protected View mRootView;
    protected ICallback mHost;

    protected boolean isFirstInit = true;

    protected String TAG = this.getClass().getSimpleName();
    private ProgressDialog mProgressDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, ":onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, ":onCreate");
        mContext = getActivity();
        mHost = (ICallback) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(getLayoutId(), null);
            ButterKnife.bind(this, mRootView);
            initView();
        } else {

            //避免重复加载UI，需要判断rootView是否被加载过parent
            //如果有parent需要删除，不然会发生rootview已有parent错误  8/7 23:05
            ViewGroup parent = (ViewGroup) mRootView.getParent();

            if (parent != null) {
                parent.removeView(mRootView);
            }
        }

        Log.d(TAG, ":onCreateView");
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, ":onActivityCreated");
        initData();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, ":onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, ":onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, ":onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, ":onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, ":onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, ":onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, ":onDetach");
    }

    public interface ICallback {
        /**
         * Fragment跟Host交互的回调
         *
         * @param obj 回调数据类型未定
         * @author hongfei.tao on 2016/8/7 10:48
         */
        void onFragmentInteraction(Object obj);

        void onDialogStateChanged(EDialogState state, String msg);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //实现数据的懒加载
        if (isVisibleToUser) {
            onVisiable(isVisibleToUser);
        } else {
            onInvisable();
        }
    }

    protected void onInvisable() {

    }

    /**
     * 当界面可见的时候回调
     *
     * @author hongfei.tao on 2016/8/7 20:16
     */
    protected void onVisiable(boolean isVisibleToUser) {
        if (isVisibleToUser && isFirstInit) {
            isFirstInit = false;

            initNetData();
        }
    }

    /**
     * 初始化网络数据
     *
     * @author hongfei.tao on 2016/8/8 0:03
     */
    protected abstract void initNetData();

    protected void showWaitDialog(String msg) {
        mHost.onDialogStateChanged(EDialogState.STATE_SHOW_DIALOG, msg);
    }

    protected void hideWaitDialog() {
        mHost.onDialogStateChanged(EDialogState.STATE_HIDE_DIALOG, "");
    }

    public enum EDialogState {
        STATE_SHOW_DIALOG,
        STATE_HIDE_DIALOG
    }

}
