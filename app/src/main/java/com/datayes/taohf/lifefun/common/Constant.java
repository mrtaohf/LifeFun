package com.datayes.taohf.lifefun.common;

import com.datayes.taohf.lifefun.R;

/**
 * Created by hongfei.tao on 2016/8/4.
 */
public class Constant {
    /**
     * 加载图片失败之后，用来展示的错误图片
     */
    public static final int ERROR_IMAGE_ID = R.drawable.ic_launcher;

    /**
     * 图片加载过程中设置的默认图片
     */
    public static final int LOADING_IMAGE_ID = R.drawable.ic_launcher;

    /**
     * 时间延迟常量
     */
    public static final int NORMAL_DELAY = 2000;

    /**
     * 网络请求分页参数
     */
    //每页默认请求数量
    public static final int PAGE_SIZE = 50;

    /**
     * 聚合数据提供的接口参数---Appkey
     */
    //微信精选
    public static final String WEIXIN_JINGXUAN_KEY = "ccbe5ab9250be0859fbd79eb4e78ef7a";

    //新闻头条
    public static final String NEWS_TOUTIAO_KEY = "0a8f7013f8f36e2bdf1d6bd53f0f5510";

    //问答机器人
    public static final String SMART_MAN_KEY = "cf5e8245be9e7fbb4e2c1e33dc1edfbf";

    //电影票房
    public static final String MOVIE_TICKER_KEY = "7e2c120f247b54edf082bb2172219b1b";

    //星座运势
    public static final String CONSTELLATION_KEY = "c1c364ba08f63f39f05bc2baaa8070ca";

    //影视播放地址解析
    public static final String MOVIE_PLAY_KEY = "93d0f4361eaa4afa664d134d1f138545";

    //足球联赛
    public static final String FOOTBALL_MATCH_KEY = "5d36f6219faa559590f7cfa3b40501a9";

    //NBA赛事
    public static final String NBA_MATCH_KEY = "3b19de66bc859c582ff73304630c2e62";

    //天气预报
    public static final String WEATHER_FORECASE_KEY = "3eaec2a41ec7eef73dd3f8d75caa2cf1";

    //全国天气预报
    public static final String NATIONAL_WEATHER_KEY = "ba3d673f7eafc5e882aed5be34bf9dbf";

    //笑话大全
    public static final String JOKES_KEY = "f88cd9be43a05826abe97b5e780bd10e";

    /**
     * 用于存储在Sp的键值
     */
    //是否第一次进入App
    public static final String KEY_FIRSTENTER_APP = "user.first.enter";

    /**
     * Activity跳转时，Intent携带参数的键值
     */
    //携带新闻URL地址的键值
    public static final String KEY_DETAIL_URL = "KEY_DETAIL_URL";

    //携带新闻标题的键值
    public static final String KEY_DETAIL_TITLE = "KEY_DETAIL_TITLE";

    /**
     * 新闻头条页面相关数据
     */
    public enum ENewsType {
//        头条，科技，娱乐，社会，国内，国际，体育，军事，财经，时尚

        TOP_NEWS(0, "top", "头条"),
        KEJI_NEWS(1, "keji", "科技"),
        YULE_NEWS(1, "yule", "娱乐"),
        SHEHUI_NEWS(1, "shehui", "社会"),
        GUONEI_NEWS(1, "guonei", "国内"),
        GUOJI_NEWS(1, "guoji", "国际"),
        TIYU_NEWS(1, "tiyu", "体育"),
        JUNSHI_NEWS(1, "junshi", "军事"),
        CAIJING_NEWS(1, "caijing", "财经"),
        SHISHANG_NEWS(1, "shishang", "时尚");

        ENewsType(int index, String type, String title) {
            mIndex = index;
            mType = type;
            mTitle = title;
        }

        private int mIndex;
        private String mType;
        private String mTitle;

        public String getType() {
            return mType;
        }

        public String getTitle() {
            return mTitle;
        }
    }
}

