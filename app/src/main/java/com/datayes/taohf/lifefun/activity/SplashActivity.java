package com.datayes.taohf.lifefun.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.util.PrefsUtils;

/**
 * Created by hongfei.tao on 2016/8/3.
 */
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        App.getInstance().getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean isFirstEnter = (boolean) PrefsUtils.get(Constant.KEY_FIRSTENTER_APP, true);

                jumpTo(isFirstEnter);
            }
        }, 2000);
    }

    private void jumpTo(boolean isFirstEnter) {
        startActivity(new Intent(this, isFirstEnter ? GuideActivity.class : MainActivity.class));
        finish();
    }

}
