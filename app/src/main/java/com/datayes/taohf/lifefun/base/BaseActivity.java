package com.datayes.taohf.lifefun.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.common.AppManager;

import butterknife.ButterKnife;

/**
 * Created by hongfei.tao on 2016/8/3.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseViewInterface {

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        AppManager.getInstance().addActivity(this);
        handleInstanceState(savedInstanceState);

        initView();
        initData();
        initNetData();
    }

    protected abstract void initNetData();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getInstance().finishActivity(this);
    }

    /**
     * 处理页面重新进来时候保存的数据
     *
     * @author hongfei.tao on 2016/8/6 14:25
     */
    protected void handleInstanceState(Bundle saveInstanceState) {

    }

    protected void showWaitDialog(String msg) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }

        if (TextUtils.isEmpty(msg)) {
            mProgressDialog.setMessage(getString(R.string.loding));
        } else {
            mProgressDialog.setMessage(msg);
        }
        mProgressDialog.show();

    }

    protected void hideWaitDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

}
