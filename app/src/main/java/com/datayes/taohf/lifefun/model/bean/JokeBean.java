package com.datayes.taohf.lifefun.model.bean;

import java.util.List;

/**
 * 笑话段子数据bean
 * Created by hongfei.tao on 2016/8/14.
 */
public class JokeBean {

    /**
     * error_code : 0
     * reason : Success
     * result : {"data":[{"content":"领导：工作进展怎么样？我：我已经用了洪荒之力了；领导：这半年工作总结一下。我：鬼知道我都经历了什么。领导：你对接下来的工作有信心吗？我：没有没有，我已经很满意了。","hashId":"d70b9db93807387283d06185ca8f32ac","unixtime":1471051431,"updatetime":"2016-08-13 09:23:51"},{"content":"每次从出租车上下来，我都要看看有没有什么东西拉在座位上，多亏了这个好习惯，我今天才捡到一部手机。","hashId":"d232b86f7e7880442e4259b67ceacb6f","unixtime":1471051431,"updatetime":"2016-08-13 09:23:51"},{"content":"我不太喜欢那些思维太过跳跃的人，前一秒还一脸认真的对我说\u201c你再靠近一点\u201d，后一秒又颤抖的改口说\u201c我就报警\u201d。","hashId":"ee44abf40bd0ba93526757712a257a2f","unixtime":1471051431,"updatetime":"2016-08-13 09:23:51"},{"content":"喜欢一个人，觉得对方不会喜欢你的情况下，不要轻易表白。你一定要等，等到对方有意中人了再去表白，这样你被拒绝的成功率就更高了。","hashId":"ddf11d5153336a9d318425e71665b87e","unixtime":1471051431,"updatetime":"2016-08-13 09:23:51"},{"content":"天气这么热，时间这么晚，我还在关心俄罗斯和乌克兰会不会打起来？朝鲜核试验还会不会发生？美国哪里有没有发生枪击事件？霍顿那煞笔是不是这届就能参加残奥会？唉，作为一个联合国成员国国家的一员，每天太累了！不说了，要好好考虑下安倍晋三的葬礼我穿红衣服去合适吗？！","hashId":"d90cba98fedb6a199906bf1fd3e57f1c","unixtime":1471051431,"updatetime":"2016-08-13 09:23:51"},{"content":"异地恋就像是热水袋，随着时间的流逝会慢慢冷却，而你能做的，就是让她多喝点热水。","hashId":"ff5e4b6b5d6083d6b58458f94ffc1228","unixtime":1471050830,"updatetime":"2016-08-13 09:13:50"},{"content":"作为体制内的一员，刚入职前辈就告诉我，两种女同事不能得罪，一是长得漂亮的，背后有很厉害的干爹；一种是长得难看的，背后有很厉害的亲爹。","hashId":"37c7c0060fa0f463069e3df026eeef0e","unixtime":1471050830,"updatetime":"2016-08-13 09:13:50"},{"content":"你为什么没女朋友？因为你没钱。你为什么会没钱？因为你玩网游。你为什么玩网游？因为你没女朋友。","hashId":"a396836200b074b7b36e2543022eef2e","unixtime":1471050830,"updatetime":"2016-08-13 09:13:50"},{"content":"作为又好看又漂亮的如花似玉的姑娘来说，除了吃和玩，更重要的当然是买了！","hashId":"cc325588effd1bc4e8d9c8dd0d641d24","unixtime":1471050830,"updatetime":"2016-08-13 09:13:50"},{"content":"原配相当于一碗健康的米饭，刚开始都时候很喜欢吃，等到熟悉了每天都吃，突然有一天吃够了，就想吃零食，零食相当于小三或者小四或者小n。尝到了新鲜的味道，就不喜欢吃米饭了，从此抛弃米饭。每天都吃零食，吃着吃着，突然有一天身体很不好过，因为零食里面添加各种化学物质，最后到医院检查，医生建议最好吃米饭，在吃零食生命会有危险！听了医生的话回家找米饭，可是到家一看，米饭已经在滋养着别人了。。。","hashId":"4482dde4e3b321ea300555b16a6fcc4d","unixtime":1471050830,"updatetime":"2016-08-13 09:13:50"},{"content":"&nbsp; &nbsp; 去年年底，跟岳父大人去钓鱼。我看浮漂下沉，就往上收杆，谁知道没拉起。我想是条大鱼，就奋力往上收。由于用力过猛，没站稳，摔倒在岳父后背上，把年过六旬的老岳父撞进了河里。岳父上岸后也没说别的，就说：\u201c这水有点凉！\u201d前几天我问岳父：\u201c爸，咱钓鱼去？\u201d岳父：\u201c我就这一个女儿，财产早晚是你的！别着急！\u201d","hashId":"70304ed03a9a40598c81bc14e12ad1cb","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"A：好无聊啊，    B：是啊，要不咱俩假装打麻将吧，    A：好的啊，那你先出B：东风，    A：胡了！单调东风！    B：.............    B：这次你先出，    A：胡了，天胡！    B：我艹你妈！！","hashId":"95b40fef15222fd9a5c35ed031a7d004","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"&nbsp; &nbsp; 现在流行上学无用论，我想说的是，在学校学的东西，都是有用的，即便是学校对你的惩罚，你也是将受益终生的。比如我，上学的时候，经常被罚站，现在，我做了一名保安，每天都站着一点都不累！","hashId":"f9c091b9c591ad8919692d5da3e4b522","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"&nbsp; &nbsp;中国跳水没意思，进水里一点波澜都没有，你看人家菲律宾，入水时惊涛骇浪，跟野猪跳江似的，看着过瘾","hashId":"24ca45494ac3b60ede0104cff3d6f48e","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"&nbsp; &nbsp; 办公室里几个年轻的男同事经常不穿工作服上班，领导逐渐也懒的管了。最近，办公室新来了一个年轻漂亮的大胸美女，她随口说了句 : \u201c你们穿着工作服的样子真帅气！\u201d第二天，他们都齐刷刷的穿着工作服来了。","hashId":"39e1e51bebb3c8205e5c53a0dbca1286","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"&nbsp; &nbsp; 刚刚突然下雨，想起楼下的电驴子，还在路边停着，赶紧下楼去推。到楼下一看，车子竟然没有了，正打算掏手机报警的瞬间，隔壁小店老板，三步并两步，跑过来跟我说：你那破驴子真矫情，落点雨砸它身上，就哇哇叫，它一叫，所有的车跟着一块叫\u2026\u2026听得人心慌意乱，我给你挪我店里了！！！","hashId":"7f1a31e1c09667e1c09d6c81d03983ea","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"&nbsp; &nbsp; &nbsp; &nbsp; 堂哥买过一根虎鞭，往酒里一泡，掉色了\u2026\u2026原来真的是一截树根。我比他更难过，特么的那酒是我买的，花了六十多块。","hashId":"451cfd827446362a1e703d8596012175","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"&nbsp; &nbsp; 正添饭呢，儿子突然就伸一个空碗过来说：来，给爷也添碗饭！我一瞪他：哪里学的，小崽子你不想活了！不是他跑得快，我腿都要踢断他的。你们以为这就完了。这时坐饭桌上我爸说话了：是我叫他添饭，你打他干什么啊\u2026\u2026","hashId":"52d5dbc9c118750fc4ab82cef3043740","unixtime":1471047831,"updatetime":"2016-08-13 08:23:51"},{"content":"有个年轻人去饭店吃饭，结帐时拿到两张50元的消费券，消费券上有时间限制，而且规定每桌一次只能使用一张券，年轻人觉得很麻烦，就把券送给了同事。　　第二天，同事喜滋滋地对年轻人说：\u201c昨天我去那家饭店吃饭，五个人才掏了70多块钱，多亏你那两张券。\u201d　　年轻人吃惊地问：\u201c两张券你一次全花了？\u201d　　同事自豪地说：\u201c那当然。我们三个人坐一桌，另外两个人一桌。\u201d　　年轻人不屑地说：\u201c分开吃，多没气氛。\u201d　　同事神气地说：\u201c我们这桌点完菜后，立即打包埋单，把券花出去后，拎着菜转身就坐另一桌去了。\u201d","hashId":"fc60a9cb6b45f60036dcd97afa084f6e","unixtime":1471046030,"updatetime":"2016-08-13 07:53:50"},{"content":"一天，雄狮先生遇见了孔雀小姐：\u201c你的美丽羽毛如果像我一样长在脖子上不更漂亮吗？\u201d　　孔雀：\u201c不，不，您见过谁把裙子当围巾用吗？\u201d　　狮子大怒：\u201c什么？你讽刺我没穿裤子？！\u201d","hashId":"90025955e4f1c9d2e2b904804d26ec24","unixtime":1471046030,"updatetime":"2016-08-13 07:53:50"}]}
     */

    private int error_code;
    private String reason;
    private ResultBean result;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * content : 领导：工作进展怎么样？我：我已经用了洪荒之力了；领导：这半年工作总结一下。我：鬼知道我都经历了什么。领导：你对接下来的工作有信心吗？我：没有没有，我已经很满意了。
         * hashId : d70b9db93807387283d06185ca8f32ac
         * unixtime : 1471051431
         * updatetime : 2016-08-13 09:23:51
         */

        private List<DataBean> data;

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            private String content;
            private String hashId;
            private long unixtime;
            private String updatetime;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getHashId() {
                return hashId;
            }

            public void setHashId(String hashId) {
                this.hashId = hashId;
            }

            public long getUnixtime() {
                return unixtime;
            }

            public void setUnixtime(long unixtime) {
                this.unixtime = unixtime;
            }

            public String getUpdatetime() {
                return updatetime;
            }

            public void setUpdatetime(String updatetime) {
                this.updatetime = updatetime;
            }
        }
    }
}
