package com.datayes.taohf.lifefun.util;

import android.widget.TextView;

/**
 * $DESC$
 * Created by hongfei.tao on 2016/8/12.
 */
public class TextViewUtils {
    public static String substring(TextView textView, String content) {
        float width = textView.getPaint().measureText(content);
        int tvWidth = textView.getLayoutParams().width;

        int lines = (int) (width / tvWidth);

        if (lines > 1) {
            int endIndex = (int) (content.length() * (1.5 / (width / tvWidth)));
            return content.substring(0, endIndex).concat("...");
        }

        return content;
    }
}
