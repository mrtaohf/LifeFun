package com.datayes.taohf.lifefun.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.activity.MainActivity;
import com.datayes.taohf.lifefun.activity.DetailActivity;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.AppManager;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.model.bean.NewsBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 新闻页面适配器
 * Created by hongfei.tao on 2016/8/12.
 */
public class NewsItemAdapter extends RecyclerView.Adapter<NewsItemAdapter.NewsItemViewHolder> {

    private Context mContext;
    private List<NewsBean.ResultBean.DataBean> mDataBeanList;

    public NewsItemAdapter(Context context) {
        this.mContext = context;
    }

    public void setList(List<NewsBean.ResultBean.DataBean> list) {
        if (list != null) {
            mDataBeanList = list;
            notifyDataSetChanged();
        }
    }

    @Override
    public NewsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsItemViewHolder(View.inflate(parent.getContext(), R.layout.item_news, null));
    }

    @Override
    public void onBindViewHolder(NewsItemViewHolder holder, final int position) {
        if (mDataBeanList != null && mDataBeanList.size() > 0) {
            final NewsBean.ResultBean.DataBean bean = mDataBeanList.get(position);

            App.getInstance().simpleDisplayImage(bean.getThumbnail_pic_s(), holder.mIvImg);
            holder.mTvTitle.setText(bean.getTitle());
            holder.mTvAuthor.setText(bean.getAuthor_name());
            holder.mTvDate.setText(bean.getDate());
            holder.mRlContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AppManager.getActivity(MainActivity.class));
                    AlertDialog dialog = builder.setTitle("详情")
                            .setMessage(mDataBeanList.get(position).getTitle())
                            .setPositiveButton("查看", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    Intent intent = new Intent(mContext, DetailActivity.class);
                                    intent.putExtra(Constant.KEY_DETAIL_TITLE, bean.getTitle());
                                    intent.putExtra(Constant.KEY_DETAIL_URL, bean.getUrl());
                                    mContext.startActivity(intent);
                                }
                            })
                            .setNegativeButton("忽略", null)
                            .show();
                }
            });

            if (TextUtils.isEmpty(bean.getCategory())) {
                holder.mTvType.setText(bean.getType());
                holder.mTvCategroy.setText(bean.getRealtype());
                holder.mTvType.setVisibility(View.VISIBLE);
            } else {
                holder.mTvCategroy.setText(bean.getCategory());
                holder.mTvType.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataBeanList.size();
    }

    static class NewsItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_img)
        ImageView mIvImg;
        @BindView(R.id.tv_news_title)
        TextView mTvTitle;
        @BindView(R.id.tv_news_author)
        TextView mTvAuthor;
        @BindView(R.id.tv_news_date)
        TextView mTvDate;
        @BindView(R.id.tv_news_categroy)
        TextView mTvCategroy;
        @BindView(R.id.tv_news_type)
        TextView mTvType;
        @BindView(R.id.rl_news_item_container)
        RelativeLayout mRlContainer;

        public NewsItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
