package com.datayes.taohf.lifefun.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.adapter.WeixinAdapter;
import com.datayes.taohf.lifefun.adapter.WeixinPagerAdapter;
import com.datayes.taohf.lifefun.base.BaseActivity;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.common.FindMenu;
import com.datayes.taohf.lifefun.model.bean.WeixinBean;
import com.datayes.taohf.lifefun.network.NetConfig;
import com.datayes.taohf.lifefun.network.service.WeixinService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeixinActivity extends BaseActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, Callback<WeixinBean> {

    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;
    //    @BindView(R.id.vp_weixin)
//    ViewPager mViewPager;
    @BindView(R.id.rv_list_weixin)
    RecyclerView mRecyclerView;
    @BindView(R.id.srl_refresh_weixin)
    SwipeRefreshLayout mRefreshLayout;

    //请求参数集合
    private Map<String, String> mParams;

    //请求参数键值
    private String pno;
    private String ps;
    private String dtype;
    private String key;

    //当前请求参数值
    private int mCurrPageIndex;
    private int mPageSize;
    private String mDataType;
    private String mKey;

    private List<WeixinBean.ResultBean.ListBean> mWeixinList;
    private List<WeixinBean.ResultBean.ListBean> mVpList;
    private List<WeixinBean.ResultBean.ListBean> mTempList;

    private WeixinService mService;
    private WeixinPagerAdapter mVpAdapter;
    private WeixinAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_weixin;
    }

    @Override
    public void initView() {
        mToolbar.setTitle(FindMenu.WEIXIN.getTitle());
        mToolbar.setNavigationContentDescription(FindMenu.WEIXIN.getTitle());
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.back_icon);
        mToolbar.setNavigationOnClickListener(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new WeixinAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.B1, R.color.B2, R.color.R1, R.color.Z1);
    }

    @Override
    public void initData() {
        mWeixinList = new ArrayList<>();
        mVpList = new ArrayList<>();

        pno = "pno";
        ps = "ps";
        dtype = "dtype";
        key = "key";

        mCurrPageIndex = 1;
        mDataType = "json";
        mPageSize = Constant.PAGE_SIZE;
        mKey = Constant.WEIXIN_JINGXUAN_KEY;

        mParams = new HashMap<>();
        mParams.put(pno, String.valueOf(mCurrPageIndex));
        mParams.put(ps, String.valueOf(mPageSize));
        mParams.put(dtype, mDataType);
        mParams.put(key, mKey);
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void onRefresh() {
        showWaitDialog("");
        mCurrPageIndex = 1;
        initNetData();
    }

    @Override
    protected void initNetData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NetConfig.NetConfigInfo.WEIXIN_JINGXUAN.getBaseUrl())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mService = retrofit.create(WeixinService.class);
        Call<WeixinBean> weixinCall = mService.listWeixin(mParams);
        weixinCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<WeixinBean> call, Response<WeixinBean> response) {
        hideWaitDialog();
        mRefreshLayout.setRefreshing(false);
        mTempList = response.body().getResult().getList();

        if (mTempList != null && mTempList.size() > 0) {
            /*if (mCurrPageIndex == 1 && mTempList.size() > 5) {
                for (int i = 0; i < 5; i++) {
                    WeixinBean.ResultBean.ListBean listBean = mTempList.remove(i);
                    mVpList.add(listBean);


                }
            }*/
            mWeixinList.addAll(mTempList);
            mAdapter.setList(mWeixinList);
        } else {

        }
    }

    @Override
    public void onFailure(Call<WeixinBean> call, Throwable t) {
        hideWaitDialog();
        mRefreshLayout.setRefreshing(false);
    }
}
