package com.datayes.taohf.lifefun.common;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.Arrays;
import java.util.List;

/**
 * Created by hongfei.tao on 2016/8/3.
 */
public class App extends Application {

    private static Context sContext;
    private static Handler sHandler;
    private static Thread sMainThread;
    private static int sMainThreadId;

    private static App instance_;

    @Override
    public void onCreate() {
        super.onCreate();

        instance_ = this;
        sContext = getApplicationContext();
        sHandler = new Handler(Looper.myLooper());
        sMainThread = Thread.currentThread();
        sMainThreadId = android.os.Process.myTid();
    }

    public static App getInstance() {
        return instance_;
    }

    public Context getContext() {
        return sContext;
    }

    public Handler getHandler() {
        return sHandler;
    }

    public Thread getMainThread() {
        return sMainThread;
    }

    public int getMainThreadId() {
        return sMainThreadId;
    }

    public List<String> getStringArray(int resId) {
        return Arrays.asList(getContext().getResources().getStringArray(resId));
    }

    /**
     * 获取显示属性
     *
     * @return
     */
    public DisplayMetrics getDisplayMetrics() {
        return getContext().getResources().getDisplayMetrics();
    }

    /**
     * 根据传入的dp值生成像素值
     *
     * @param dipValue
     * @return
     */
    public int dip2px(float dipValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, getDisplayMetrics());
    }

    /**
     * 根据传入的sp值生成像素值
     *
     * @param spValue
     * @return
     */
    public int sp2px(float spValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue, getDisplayMetrics());
    }

    /**
     * 获取屏幕高度
     *
     * @author hongfei.tao on 2016/8/12 22:23
     */
    public int getWindowHeight() {
        return getDisplayMetrics().widthPixels;
    }

    /**
     * 获取屏幕的宽度
     *
     * @author hongfei.tao on 2016/8/12 22:23
     */
    public int getWindowWidth() {
        return getDisplayMetrics().widthPixels;
    }

    /**
     * 获取状态栏高度
     *
     * @author hongfei.tao on 2016/8/12 22:27
     */
    public int getStatusHeight() {
        int statusHeight = -1;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusHeight = getContext().getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }

    /**
     * 展示网络秃图片
     *
     * @param uri 图片地址
     * @param iv  显示控件
     */
    public void simpleDisplayImage(String uri, ImageView iv) {
        Glide.with(sContext)
                .load(uri)
                .centerCrop()
                .into(iv);
    }

    /**
     * 展示网络图片
     *
     * @param uri               图片地址
     * @param iv                显示的控件
     * @param isSkipMemoryCache 内存缓存策略
     * @param diskCacheType     磁盘缓存策略
     */
    public void displayImage(String uri, ImageView iv, boolean isSkipMemoryCache, DiskCacheStrategy diskCacheType) {
        Glide.with(sContext)
                .load(uri)
                .placeholder(Constant.LOADING_IMAGE_ID)//加载过程中显示的图片
                .error(Constant.ERROR_IMAGE_ID)
                .centerCrop()//填充
                .skipMemoryCache(isSkipMemoryCache)
                .diskCacheStrategy(diskCacheType)
                .into(iv);
    }

    public void displayImage(String uri, ImageView iv) {
        displayImage(uri, iv, false, DiskCacheStrategy.ALL);
    }

    public void displayImage(String uri, ImageView iv, boolean isSkipMemoryCache) {
        displayImage(uri, iv, isSkipMemoryCache, DiskCacheStrategy.ALL);
    }

    public void displayImage(String uri, ImageView iv, DiskCacheStrategy diskCacheType) {
        displayImage(uri, iv, false, diskCacheType);
    }
}
