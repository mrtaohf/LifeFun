package com.datayes.taohf.lifefun.network.service;

import com.datayes.taohf.lifefun.model.bean.WeixinBean;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * 微信精选业务层
 * Created by hongfei.tao on 2016/8/14.
 */
public interface WeixinService {

    @GET("weixin/query")
    Call<WeixinBean> listWeixin(@QueryMap Map<String, String> params);

}
