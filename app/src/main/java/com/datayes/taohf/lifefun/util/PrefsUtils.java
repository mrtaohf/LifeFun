package com.datayes.taohf.lifefun.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.SharedPreferencesCompat;

import com.datayes.taohf.lifefun.common.App;

import java.util.Map;

/**
 * Created by hongfei.tao on 2016/8/4.
 */
public class PrefsUtils {

    private static SharedPreferences sSharedPreferences;

    private static final String SP_NAME = "prefs_config";

    private static final int SP_MODE = Context.MODE_PRIVATE;

    private static SharedPreferences getSharedPreferences() {
        if (sSharedPreferences == null) {
            sSharedPreferences = App.getInstance().getContext().getSharedPreferences(SP_NAME, SP_MODE);
        }

        return sSharedPreferences;
    }

    private static SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

    /**
     * 向sp存入值
     *
     * @param key
     * @param value
     */
    public static void put(String key, Object value) {
        SharedPreferences.Editor editor = getEditor();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else {
            editor.putString(key, value.toString());
        }

        apply(editor);
    }

    /**
     * 从sp获取值
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public static Object get(String key, Object defaultValue) {
        if (defaultValue instanceof String) {
            return getSharedPreferences().getString(key, (String) defaultValue);
        } else if (defaultValue instanceof Integer) {
            return getSharedPreferences().getInt(key, (Integer) defaultValue);
        } else if (defaultValue instanceof Float) {
            return getSharedPreferences().getFloat(key, (Float) defaultValue);
        } else if (defaultValue instanceof Long) {
            return getSharedPreferences().getLong(key, (Long) defaultValue);
        } else if (defaultValue instanceof Boolean) {
            return getSharedPreferences().getBoolean(key, (Boolean) defaultValue);
        }

        return null;
    }

    /**
     * 从sp中移除某个键所对应的值
     *
     * @param key
     */
    public static void remove(String key) {
        SharedPreferences.Editor editor = getEditor().remove(key);
        apply(editor);
    }

    /**
     * 清空Sp
     */
    public static void clear() {
        SharedPreferences.Editor editor = getEditor().clear();
        apply(editor);
    }

    /**
     * 获取存储在sp的所有数据
     *
     * @return
     */
    public static Map<String, ?> getAll() {
        return getSharedPreferences().getAll();
    }

    /**
     * 判断sp是否包含指定的键
     *
     * @param key
     * @return
     */
    public static boolean contains(String key) {
        return getSharedPreferences().contains(key);
    }

    private static void apply(SharedPreferences.Editor editor) {
        SharedPreferencesCompat.EditorCompat.getInstance().apply(editor);
    }
}
