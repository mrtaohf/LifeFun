package com.datayes.taohf.lifefun.fragment;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.base.BaseFragment;

/**
 * "我的"页面
 * Created by hongfei.tao on 2016/8/5.
 */
public class MineFragment extends BaseFragment {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void onVisiable(boolean isVisibleToUser) {

    }

    @Override
    protected void initNetData() {

    }
}
