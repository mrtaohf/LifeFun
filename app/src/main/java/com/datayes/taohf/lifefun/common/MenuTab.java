package com.datayes.taohf.lifefun.common;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.fragment.FindFragment;
import com.datayes.taohf.lifefun.fragment.MatchFragment;
import com.datayes.taohf.lifefun.fragment.MineFragment;
import com.datayes.taohf.lifefun.fragment.NewsFragment;
import com.datayes.taohf.lifefun.fragment.VideoFragment;

/**
 * 主界面tab枚举
 * Created by hongfei.tao on 2016/8/6.
 */
public enum MenuTab {

    TAB_NEWS(0, R.drawable.tab_icon_news, R.string.news, NewsFragment.class),
    TAB_Video(1, R.drawable.tab_icon_video, R.string.video, VideoFragment.class),
    TAB_MATCH(2, R.drawable.tab_icon_match, R.string.match, MatchFragment.class),
    TAB_FIND(2, R.drawable.tab_icon_find, R.string.find, FindFragment.class),
    TAB_MINE(3, R.drawable.tab_icon_mine, R.string.mine, MineFragment.class);

    private int index;
    private int icon;
    private int tabText;
    private Class<? extends BaseFragment> clazz;

    MenuTab(int index, int icon, int tabText, Class<? extends BaseFragment> clazz) {
        this.index = index;
        this.icon = icon;
        this.tabText = tabText;
        this.clazz = clazz;
    }

    public int getIndex() {
        return index;
    }

    public int getIcon() {
        return icon;
    }

    public int getTabText() {
        return tabText;
    }

    public Class<? extends BaseFragment> getClazz() {
        return clazz;
    }
}
