package com.datayes.taohf.lifefun.model.bean;

import java.util.List;

/**
 * 微信精选bean
 * Created by hongfei.tao on 2016/8/13.
 */
public class WeixinBean {

    /**
     * reason : success
     * result : {"list":[{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7405458.jpg/640","id":"wechat_20160813026163","source":"心灵鸡汤吧","title":"少在我背后谈论我，闭上你那欠扁的嘴。","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026163","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-6253735.jpg/640","id":"wechat_20160813026164","source":"心灵鸡汤吧","title":"晚安心语：心事不必说给每个人听","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026164","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7405448.jpg/640","id":"wechat_20160813026155","source":"每日经济新闻","title":"在40的高铁车厢闷坐2小时，晚点还没赔偿？看看日本是怎么做的","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026155","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7335810.jpg/640","id":"wechat_20160813025903","source":"娱美人儿","title":"赵丽颖居然牵扯进了叶青姚笛的骂战，这次她情商终于高了一回","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025903","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404959.jpg/640","id":"wechat_20160813025905","source":"娱美人儿","title":"葛荟婕与女摄影师恋情公布，那就不要再纠结于跟汪峰的过去啦！","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025905","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404765.jpg/640","id":"wechat_20160813025816","source":"柒弥","title":"脱掉精致生活，她去荒岛求生","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025816","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404576.jpg/640","id":"wechat_20160813025692","source":"清华南都","title":"穷人家孩子的出路到底在哪里呢","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025692","mark":""},{"firstImg":"","id":"wechat_20160813025654","source":"机车网","title":"20秒的慢镜头，告诉你摔车有多危险","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025654","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404502.jpg/640","id":"wechat_20160813025644","source":"笨鸟文摘","title":"古代妃子侍寝十大潜规则","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025644","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404499.jpg/640","id":"wechat_20160813025642","source":"笨鸟文摘","title":"中国四大著名错字，竟无一人质疑","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025642","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404484.jpg/640","id":"wechat_20160813025637","source":"历史大学堂","title":"\u201c卧薪尝胆\u201d一个被人为误读千年的励志鸡汤","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025637","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404420.jpg/640","id":"wechat_20160813025602","source":"美到心碎的散文","title":"既为过客，何不放下","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025602","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404432.jpg/640","id":"wechat_20160813025605","source":"美到心碎的散文","title":"谁笔下生花，唯美了曾经","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025605","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404291.jpg/640","id":"wechat_20160813025496","source":"乐家网","title":"必须改！手机支付10个坏习惯，可能让你倾家荡产","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025496","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404313.jpg/640","id":"wechat_20160813025497","source":"星座秘语","title":"有沟通困难症的三对星座","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025497","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404277.jpg/640","id":"wechat_20160813025480","source":"左右青春","title":"晚安|愿你与这世界坦诚相待","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025480","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404235.jpg/640","id":"wechat_20160813025472","source":"十二星座大揭秘","title":"能让你记一辈子的前男友是啥星座！","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025472","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404034.jpg/640","id":"wechat_20160813025402","source":"数字尾巴","title":"近视还想要耍酷，或许它可以帮到你~","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025402","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7403882.jpg/640","id":"wechat_20160813025384","source":"高校女神","title":"迪丽热巴当女保镖？上演制服诱惑，简直美翻了！","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025384","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7403931.jpg/640","id":"wechat_20160813025385","source":"每日七言","title":"一个人回复你的速度，取决于在乎你的程度","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025385","mark":""}],"totalPage":25,"ps":20,"pno":1}
     * error_code : 0
     */

    private String reason;
    /**
     * list : [{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7405458.jpg/640","id":"wechat_20160813026163","source":"心灵鸡汤吧","title":"少在我背后谈论我，闭上你那欠扁的嘴。","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026163","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-6253735.jpg/640","id":"wechat_20160813026164","source":"心灵鸡汤吧","title":"晚安心语：心事不必说给每个人听","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026164","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7405448.jpg/640","id":"wechat_20160813026155","source":"每日经济新闻","title":"在40的高铁车厢闷坐2小时，晚点还没赔偿？看看日本是怎么做的","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026155","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7335810.jpg/640","id":"wechat_20160813025903","source":"娱美人儿","title":"赵丽颖居然牵扯进了叶青姚笛的骂战，这次她情商终于高了一回","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025903","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404959.jpg/640","id":"wechat_20160813025905","source":"娱美人儿","title":"葛荟婕与女摄影师恋情公布，那就不要再纠结于跟汪峰的过去啦！","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025905","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404765.jpg/640","id":"wechat_20160813025816","source":"柒弥","title":"脱掉精致生活，她去荒岛求生","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025816","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404576.jpg/640","id":"wechat_20160813025692","source":"清华南都","title":"穷人家孩子的出路到底在哪里呢","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025692","mark":""},{"firstImg":"","id":"wechat_20160813025654","source":"机车网","title":"20秒的慢镜头，告诉你摔车有多危险","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025654","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404502.jpg/640","id":"wechat_20160813025644","source":"笨鸟文摘","title":"古代妃子侍寝十大潜规则","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025644","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404499.jpg/640","id":"wechat_20160813025642","source":"笨鸟文摘","title":"中国四大著名错字，竟无一人质疑","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025642","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404484.jpg/640","id":"wechat_20160813025637","source":"历史大学堂","title":"\u201c卧薪尝胆\u201d一个被人为误读千年的励志鸡汤","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025637","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404420.jpg/640","id":"wechat_20160813025602","source":"美到心碎的散文","title":"既为过客，何不放下","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025602","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404432.jpg/640","id":"wechat_20160813025605","source":"美到心碎的散文","title":"谁笔下生花，唯美了曾经","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025605","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404291.jpg/640","id":"wechat_20160813025496","source":"乐家网","title":"必须改！手机支付10个坏习惯，可能让你倾家荡产","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025496","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404313.jpg/640","id":"wechat_20160813025497","source":"星座秘语","title":"有沟通困难症的三对星座","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025497","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404277.jpg/640","id":"wechat_20160813025480","source":"左右青春","title":"晚安|愿你与这世界坦诚相待","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025480","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404235.jpg/640","id":"wechat_20160813025472","source":"十二星座大揭秘","title":"能让你记一辈子的前男友是啥星座！","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025472","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7404034.jpg/640","id":"wechat_20160813025402","source":"数字尾巴","title":"近视还想要耍酷，或许它可以帮到你~","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025402","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7403882.jpg/640","id":"wechat_20160813025384","source":"高校女神","title":"迪丽热巴当女保镖？上演制服诱惑，简直美翻了！","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025384","mark":""},{"firstImg":"http://zxpic.gtimg.com/infonew/0/wechat_pics_-7403931.jpg/640","id":"wechat_20160813025385","source":"每日七言","title":"一个人回复你的速度，取决于在乎你的程度","url":"http://v.juhe.cn/weixin/redirect?wid=wechat_20160813025385","mark":""}]
     * totalPage : 25
     * ps : 20
     * pno : 1
     */

    private ResultBean result;
    private int error_code;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public static class ResultBean {
        private int totalPage;
        private int ps;
        private int pno;
        /**
         * firstImg : http://zxpic.gtimg.com/infonew/0/wechat_pics_-7405458.jpg/640
         * id : wechat_20160813026163
         * source : 心灵鸡汤吧
         * title : 少在我背后谈论我，闭上你那欠扁的嘴。
         * url : http://v.juhe.cn/weixin/redirect?wid=wechat_20160813026163
         * mark :
         */

        private List<ListBean> list;

        public int getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }

        public int getPs() {
            return ps;
        }

        public void setPs(int ps) {
            this.ps = ps;
        }

        public int getPno() {
            return pno;
        }

        public void setPno(int pno) {
            this.pno = pno;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            private String firstImg;
            private String id;
            private String source;
            private String title;
            private String url;
            private String mark;

            public String getFirstImg() {
                return firstImg;
            }

            public void setFirstImg(String firstImg) {
                this.firstImg = firstImg;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getMark() {
                return mark;
            }

            public void setMark(String mark) {
                this.mark = mark;
            }
        }
    }
}
