package com.datayes.taohf.lifefun.network.service;

import com.datayes.taohf.lifefun.model.bean.NewsBean;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * 新闻接口的业务类
 * Created by hongfei.tao on 2016/8/7.
 */
public interface NewsService {

    @GET("toutiao/index")
    Call<NewsBean> listNews(@QueryMap Map<String, String> params);

}
