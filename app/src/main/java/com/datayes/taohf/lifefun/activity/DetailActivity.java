package com.datayes.taohf.lifefun.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.base.BaseActivity;
import com.datayes.taohf.lifefun.common.Constant;

import butterknife.BindView;

/**
 * 新闻详情页面
 * Created by hongfei.tao on 2016/8/13.
 */
public class DetailActivity extends BaseActivity implements View.OnClickListener, Toolbar.OnMenuItemClickListener {

    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.wv_news_detail)
    WebView mWebView;

    private String mNewsUrl;

    @Override
    public int getLayoutId() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void initView() {
        mToolbar.setNavigationContentDescription("新闻详情");
        mToolbar.setTitle("新闻详情");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.back_icon);
        mToolbar.setNavigationOnClickListener(this);
//        mToolbar.setOnMenuItemClickListener(this);

        showWaitDialog("看官，精彩内容正在加载，请稍等哦~");
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            mToolbar.setSubtitle(intent.getStringExtra(Constant.KEY_DETAIL_TITLE));
            mToolbar.setSubtitleTextAppearance(this, R.style.MarqueeTextView);
            mNewsUrl = intent.getStringExtra(Constant.KEY_DETAIL_URL);

            if (!TextUtils.isEmpty(mNewsUrl)) {
                mWebView.getSettings().setJavaScriptEnabled(true);
                mWebView.loadUrl(mNewsUrl);
                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        hideWaitDialog();
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    protected void initNetData() {

    }
}
