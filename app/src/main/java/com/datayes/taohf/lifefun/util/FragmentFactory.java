package com.datayes.taohf.lifefun.util;

import android.util.Log;

import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.fragment.FindFragment;
import com.datayes.taohf.lifefun.fragment.MineFragment;
import com.datayes.taohf.lifefun.fragment.NewsFragment;
import com.datayes.taohf.lifefun.fragment.VideoFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hongfei.tao on 2016/8/5.
 */
public class FragmentFactory {

    public static final String TAG = FragmentFactory.class.getSimpleName();

    private static Map<String, BaseFragment> mFragments = new HashMap<>();

    public static BaseFragment createFragment(String tag) {
        BaseFragment fragment = mFragments.get(tag);

        try {

            if (fragment == null) {
                switch (tag) {
                    case "NewsFragment":
                        fragment = EFragmentType.NEWS_FRAGMNET.getFragment().newInstance();
                        break;
                    case "VideoFragment":
                        fragment = EFragmentType.VEDIO_FRAGMENT.getFragment().newInstance();
                        break;
                    case "FindFragment":
                        fragment = EFragmentType.FIND_FRAGMENT.getFragment().newInstance();
                        break;
                    case "MineFragment":
                        fragment = EFragmentType.MINE_FRAGMENT.getFragment().newInstance();
                        break;
                }
                fragment = EFragmentType.NEWS_FRAGMNET.getFragment().newInstance();

                mFragments.put(tag, fragment);
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        return fragment;
    }

    enum EFragmentType {

        NEWS_FRAGMNET(NewsFragment.class, "新闻页面"),
        VEDIO_FRAGMENT(VideoFragment.class, "视频页面"),
        FIND_FRAGMENT(FindFragment.class, "发现页面"),
        MINE_FRAGMENT(MineFragment.class, "我的页面");

        private Class<? extends BaseFragment> mFragmentClazz;

        public Class<? extends BaseFragment> getFragment() {
            return mFragmentClazz;
        }

        EFragmentType(Class<? extends BaseFragment> fragment, String desc) {
            this.mFragmentClazz = fragment;
        }
    }
}
