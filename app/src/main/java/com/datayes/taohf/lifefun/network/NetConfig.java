package com.datayes.taohf.lifefun.network;

/**
 * Created by hongfei.tao on 2016/8/4.
 */
public class NetConfig {

    public static final String BASE_JUHE_URL_V = "http://v.juhe.cn/";

    public static final String BASE_JUHE_URL_JAPI = "http://japi.juhe.cn/";

    public static final String BASE_JUHE_URL_OP = "http://op.juhe.cn/";

    public static final String BASE_JUHE_URL_WEB = "http://web.juhe.cn:8080/";

    public enum NetConfigInfo {

        WEIXIN_JINGXUAN("weixin/query", BASE_JUHE_URL_V, "微信精选"),
        NEWS_TOUTIAO("toutiao/index", BASE_JUHE_URL_V, "新闻头条"),
        JOKES_DAQUAN("joke/content/list.from", BASE_JUHE_URL_JAPI, "笑话大全"),
        WEATHRE_FORECAST("onebox/weather/query", BASE_JUHE_URL_OP, "天气预报"),
        NBA_MATCH("onebox/basketball/nba", BASE_JUHE_URL_OP, "NBA赛事"),
        FOOTBALL_MATCH("onebox/football/league", BASE_JUHE_URL_OP, "足球联赛"),
        CONSTELLATION("constellation/getAll", BASE_JUHE_URL_WEB, "星座运势");

        private String mPath;
        private String mBaseUrl;
        private String mDesc;

        public String getPath() {
            return mPath;
        }

        public String getBaseUrl() {
            return mBaseUrl;
        }

        NetConfigInfo(String path, String baseUrl, String desc) {
            this.mPath = path;
            this.mDesc = desc;
            this.mBaseUrl = baseUrl;
        }
    }


}
