package com.datayes.taohf.lifefun.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.common.FragmentFactory;
import com.datayes.taohf.lifefun.fragment.NewsItemFragment;

import java.util.List;

/**
 * 新闻页面适配器
 * Created by hongfei.tao on 2016/8/8.
 */
public class NewsAdapter extends FragmentPagerAdapter {
    private List<String> mTabTitleList;

    public NewsAdapter(FragmentManager fm, List<String> list) {
        super(fm);
        mTabTitleList = list;
    }


    @Override
    public int getCount() {
        return mTabTitleList.size();
    }

    @Override
    public Fragment getItem(int position) {
        NewsItemFragment fragment = (NewsItemFragment) FragmentFactory.createFragment(mTabTitleList.get(position));
        Constant.ENewsType[] newsTypes = Constant.ENewsType.values();
        fragment.setNewsType(newsTypes[position]);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitleList.get(position);
    }

}
