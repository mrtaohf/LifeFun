package com.datayes.taohf.lifefun.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.adapter.NewsAdapter;
import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.common.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * "新闻"页面
 * Created by hongfei.tao on 2016/8/5.
 */
public class NewsFragment extends BaseFragment {

    @BindView(R.id.tl_tab_indicator_news)
    TabLayout mTabIndicator;
    @BindView(R.id.vp_container_news)
    ViewPager mVpContainer;

    private List<String> mTabTitleList;
    private NewsAdapter mNewsAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_news;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {
        if (mTabTitleList == null || mTabTitleList.isEmpty()) {
            mTabTitleList = new ArrayList();
            Constant.ENewsType[] eNewsTypes = Constant.ENewsType.values();
            if (eNewsTypes.length > 0) {
                for (int i = 0; i < eNewsTypes.length; i++) {
                    mTabTitleList.add(eNewsTypes[i].getTitle());
                }
            }
        }

        if (mNewsAdapter == null) {
            mNewsAdapter = new NewsAdapter(getChildFragmentManager(), mTabTitleList);
        }

        mVpContainer.setAdapter(mNewsAdapter);
        mTabIndicator.setupWithViewPager(mVpContainer);
    }

    @Override
    protected void initNetData() {

    }


}
