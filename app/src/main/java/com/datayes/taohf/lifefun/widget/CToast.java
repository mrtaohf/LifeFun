package com.datayes.taohf.lifefun.widget;

import android.content.Context;
import android.widget.Toast;

/**
 * 避免多次弹出Toast提示
 * Created by hongfei.tao on 2016/8/7.
 */
public class CToast {
    private static Toast mToast;

    public static void showToast(Context ctx, CharSequence text, int duration) {
        if (mToast == null) {
            mToast = Toast.makeText(ctx, text, duration);
        } else {
            mToast.setText(text);
            mToast.setDuration(duration);
        }

        mToast.show();
    }

}
