package com.datayes.taohf.lifefun.fragment;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.base.BaseFragment;

/**
 * $DESC$
 * Created by hongfei.tao on 2016/8/7.
 */
public class MatchFragment extends BaseFragment {
    @Override
    public int getLayoutId() {
        return R.layout.fragment_match;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void onVisiable(boolean isVisibleToUser) {

    }

    @Override
    protected void initNetData() {

    }
}
