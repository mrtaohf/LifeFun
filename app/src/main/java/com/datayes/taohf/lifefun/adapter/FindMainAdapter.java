package com.datayes.taohf.lifefun.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.activity.MainActivity;
import com.datayes.taohf.lifefun.common.AppManager;
import com.datayes.taohf.lifefun.common.FindMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * $DESC$
 * Created by hongfei.tao on 2016/8/14.
 */
public class FindMainAdapter extends RecyclerView.Adapter<FindMainAdapter.FindViewHolder> {

    private List<FindMenu> mMenuList;

    public FindMainAdapter(FindMenu[] menus) {
        mMenuList = new ArrayList<>();
        if (menus.length > 0) {
            for (int i = 0; i < menus.length; i++) {
                mMenuList.add(menus[i]);
            }
        }
    }

    @Override
    public FindViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FindViewHolder(View.inflate(parent.getContext(), R.layout.item_find, null));
    }

    @Override
    public void onBindViewHolder(FindViewHolder holder, int position) {
        final FindMenu menuItem = mMenuList.get(position);

        holder.mIvIcon.setImageResource(menuItem.getIcon());
        holder.mTvTitle.setText(menuItem.getTitle());
        holder.mViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppManager.getActivity(MainActivity.class), menuItem.getActivity());
                AppManager.getActivity(MainActivity.class).startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mMenuList != null && mMenuList.size() > 0) {
            return mMenuList.size();
        }

        return 0;
    }

    static class FindViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_find_item_container)
        RelativeLayout mViewContainer;
        @BindView(R.id.iv_find_menu_icon)
        ImageView mIvIcon;
        @BindView(R.id.tv_title_find)
        TextView mTvTitle;

        public FindViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
