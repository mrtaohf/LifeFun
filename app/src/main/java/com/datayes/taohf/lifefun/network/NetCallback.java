package com.datayes.taohf.lifefun.network;

/**
 * retrofit返回的数据业务类
 * Created by hongfei.tao on 2016/8/7.
 */
public interface NetCallback {

    void onNetSuccessed();

    BaseService initService(/*String baseUrl, */Class<? extends BaseService> service);

    public interface BaseService {

    }
}
