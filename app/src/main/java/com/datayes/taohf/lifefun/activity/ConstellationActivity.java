package com.datayes.taohf.lifefun.activity;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.base.BaseActivity;

/**
 * $DESC$
 * Created by hongfei.tao on 2016/8/14.
 */
public class ConstellationActivity extends BaseActivity {
    @Override
    public int getLayoutId() {
        return R.layout.activity_constellation;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void initNetData() {

    }
}
