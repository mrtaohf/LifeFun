package com.datayes.taohf.lifefun.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.adapter.JokeAdapter;
import com.datayes.taohf.lifefun.base.BaseActivity;
import com.datayes.taohf.lifefun.common.FindMenu;
import com.datayes.taohf.lifefun.model.bean.JokeBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 笑话段子页面
 * Created by hongfei.tao on 2016/8/14.
 */
public class JokeActivity extends BaseActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.rv_list_joke)
    RecyclerView mRecyclerView;
    @BindView(R.id.srl_freshlayout_joke)
    SwipeRefreshLayout mRefreshLayout;

    private List<JokeBean.ResultBean.DataBean> mJokeList;
    private JokeAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_joke;
    }

    @Override
    public void initView() {
        mToolbar.setNavigationIcon(R.mipmap.back_icon);
        mToolbar.setTitle(FindMenu.XIAOHUA.getTitle());
        mToolbar.setNavigationOnClickListener(this);
        setSupportActionBar(mToolbar);

        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.B1, R.color.B2, R.color.R1, R.color.Z1);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void initData() {
        mJokeList = new ArrayList<>();
        mAdapter = new JokeAdapter(this, mJokeList);
    }

    @Override
    protected void initNetData() {

    }

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void onRefresh() {

    }
}
