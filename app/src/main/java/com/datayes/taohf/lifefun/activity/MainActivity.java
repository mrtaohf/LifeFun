package com.datayes.taohf.lifefun.activity;

import android.support.v4.app.FragmentTabHost;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.base.BaseActivity;
import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.MenuTab;
import com.datayes.taohf.lifefun.widget.CToast;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements TabHost.OnTabChangeListener,
        BaseFragment.ICallback, Toolbar.OnMenuItemClickListener {

    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;
    @BindView(android.R.id.tabcontent)
    FrameLayout mTabcontent;
    @BindView(android.R.id.tabs)
    TabWidget mTabs;
    @BindView(android.R.id.tabhost)
    FragmentTabHost mTabhost;

    private String mTitle;

    private boolean isExit = false;

    private Runnable mExitTask = new Runnable() {
        @Override
        public void run() {
            isExit = false;
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {

        initTabs();

        initToolbar();
    }

    private void initTabs() {
        mTitle = getString(R.string.news);
        mTabhost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        MenuTab[] menuTabs = MenuTab.values();
        for (int i = 0; i < menuTabs.length; i++) {
            MenuTab menuTab = menuTabs[i];
            TabHost.TabSpec tabSpec = mTabhost.newTabSpec(getString(menuTab.getTabText()));
            View indicator = View.inflate(this, R.layout.layout_menu_tab_item, null);
            ImageView ivTabIcon = (ImageView) indicator.findViewById(R.id.iv_tab_icon);
            TextView tvTabText = (TextView) indicator.findViewById(R.id.tv_tab_text);

            ivTabIcon.setImageResource(menuTab.getIcon());
            tvTabText.setText(menuTab.getTabText());
            tabSpec.setIndicator(indicator);
            tabSpec.setContent(new TabHost.TabContentFactory() {
                @Override
                public View createTabContent(String tag) {
                    return new View(MainActivity.this);
                }
            });
            mTabhost.addTab(tabSpec, menuTab.getClazz(), null);
        }

        mTabhost.setCurrentTab(0);
        mTabhost.setOnTabChangedListener(this);
    }

    private void initToolbar() {
        mToolbar.setNavigationContentDescription(mTitle);
        mToolbar.setTitle(mTitle);
        setSupportActionBar(mToolbar);
        mToolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public void initData() {

    }


    @Override
    public void onTabChanged(String tabId) {
        for (int i = 0; i < mTabhost.getTabWidget().getTabCount(); i++) {
            View tabView = mTabhost.getTabWidget().getChildAt(i);
            if (i == mTabhost.getCurrentTab()) {
                tabView.setSelected(true);
            } else {
                tabView.setSelected(false);
            }
        }

        mTitle = tabId;
        updateToolbar();
    }

    private void updateToolbar() {
        mToolbar.setTitle(mTitle);
    }

    @Override
    public void onFragmentInteraction(Object obj) {

    }

    @Override
    public void onDialogStateChanged(BaseFragment.EDialogState state, String msg) {
        switch (state) {
            case STATE_SHOW_DIALOG:
                showWaitDialog(msg);
                break;
            case STATE_HIDE_DIALOG:
                hideWaitDialog();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu:
                CToast.showToast(this, item.getTitle(), Toast.LENGTH_SHORT);
                break;
        }

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (isExit) {

            return super.onKeyDown(keyCode, event);
        } else {
            isExit = true;
            CToast.showToast(this, getString(R.string.exit_app_tip), Toast.LENGTH_SHORT);

            App.getInstance().getHandler().postDelayed(mExitTask, 2000);

            return true;
        }

    }

    @Override
    protected void initNetData() {

    }
}
