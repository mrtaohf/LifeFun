package com.datayes.taohf.lifefun.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.adapter.NewsItemAdapter;
import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.model.bean.NewsBean;
import com.datayes.taohf.lifefun.network.NetConfig;
import com.datayes.taohf.lifefun.network.service.NewsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hongfei.tao on 2016/8/8 14:39.
 */
public class NewsItemFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_list_content)
    RecyclerView mRecyclerView;
    @BindView(R.id.srl_refresh)
    SwipeRefreshLayout mRefreshLayout;

    private NewsService mService;
    private Constant.ENewsType mNewsType;
    private Call<NewsBean> mNewsCall;

    private List<NewsBean.ResultBean.DataBean> mNewsList;
    private NewsItemAdapter mAdapter;

    @Override
    protected void initNetData() {
        showWaitDialog("");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NetConfig.NetConfigInfo.NEWS_TOUTIAO.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        mService = retrofit.create(NewsService.class);
        Map<String, String> params = new HashMap<>();
        params.put("key", Constant.NEWS_TOUTIAO_KEY);
        params.put("type", mNewsType.getType());
        mNewsCall = mService.listNews(params);

        mNewsCall.enqueue(new Callback<NewsBean>() {
            @Override
            public void onResponse(Call<NewsBean> call, Response<NewsBean> response) {
                hideWaitDialog();
                mRefreshLayout.setRefreshing(false);

                mNewsList = response.body().getResult().getData();
                mAdapter.setList(mNewsList);

            }

            @Override
            public void onFailure(Call<NewsBean> call, Throwable t) {
                showWaitDialog("获取数据失败哦~");
                mRefreshLayout.setRefreshing(false);
                App.getInstance().getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideWaitDialog();
                    }
                }, 2000);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_vp_news_item;
    }

    @Override
    public void initView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new NewsItemAdapter(getActivity());
        mAdapter.setList(new ArrayList<NewsBean.ResultBean.DataBean>());
        mRecyclerView.setAdapter(mAdapter);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.B1, R.color.B2, R.color.R1, R.color.Z1);
    }

    @Override
    public void initData() {

    }

    public void setNewsType(Constant.ENewsType type) {
        mNewsType = type;
    }

    @Override
    public void onRefresh() {
        initNetData();
    }
}
