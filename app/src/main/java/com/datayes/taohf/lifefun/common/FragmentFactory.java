package com.datayes.taohf.lifefun.common;

import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.fragment.NewsItemFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * 暂时用来缓存新闻界面的Fragment
 * Created by hongfei.tao on 2016/8/8 14:49.
 */
public class FragmentFactory {
    private static Map<String, BaseFragment> mCache = new HashMap<>();

    public static BaseFragment createFragment(String tag) {
        BaseFragment fragment = mCache.get(tag);

        if (fragment == null) {
            fragment = new NewsItemFragment();
            mCache.put(tag, fragment);
        }

        return fragment;
    }
}
