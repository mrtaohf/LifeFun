package com.datayes.taohf.lifefun.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.adapter.FindMainAdapter;
import com.datayes.taohf.lifefun.base.BaseFragment;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.common.FindMenu;

import butterknife.BindView;

/**
 * "发现"页面
 * Created by hongfei.tao on 2016/8/5.
 */
public class FindFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_list_find)
    RecyclerView mRecyclerView;
    @BindView(R.id.srl_refresh_find)
    SwipeRefreshLayout mRefreshLayout;

    private FindMainAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_find;
    }

    @Override
    public void initView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.B1, R.color.B2, R.color.R1, R.color.Z1);
    }

    @Override
    public void initData() {
        FindMenu[] findMenus = FindMenu.values();
        mAdapter = new FindMainAdapter(findMenus);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onVisiable(boolean isVisibleToUser) {

    }

    @Override
    protected void initNetData() {

    }

    @Override
    public void onRefresh() {
        showWaitDialog("亲爱的,已经是最新数据喽~");
        App.getInstance().getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(false);
                hideWaitDialog();
            }
        }, Constant.NORMAL_DELAY);
    }
}
