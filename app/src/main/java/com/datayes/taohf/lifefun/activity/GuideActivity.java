package com.datayes.taohf.lifefun.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.adapter.GuideAdapter;
import com.datayes.taohf.lifefun.common.App;
import com.datayes.taohf.lifefun.common.Constant;
import com.datayes.taohf.lifefun.util.PrefsUtils;
import com.datayes.taohf.lifefun.widget.ViewPagerIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuideActivity extends AppCompatActivity implements GuideAdapter.CallBack {

//    //没有合适图片资源，暂时不用图片来展示
//    private static final int[] GUIDE_IMAGES = {
//            R.mipmap.guide_1,
//            R.mipmap.guide_2,
//            R.mipmap.guide_3
//    };

    @BindView(R.id.vp_guide)
    ViewPager mVpGuide;
    @BindView(R.id.vpi_indicator)
    ViewPagerIndicator mViewPagerIndicator;

    private GuideAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);

        PrefsUtils.put(Constant.KEY_FIRSTENTER_APP, false);

        mAdapter = new GuideAdapter(this, App.getInstance().getStringArray(R.array.guide_arr));
        mVpGuide.setAdapter(mAdapter);
        mViewPagerIndicator.setupWithViewPager(mVpGuide);
    }

    @Override
    public void onBtnClick() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
