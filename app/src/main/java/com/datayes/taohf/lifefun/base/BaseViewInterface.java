package com.datayes.taohf.lifefun.base;

/**
 * Activity和Fragment用来完成初始化界面的接口
 * Created by hongfei.tao on 2016/8/6.
 */
public interface BaseViewInterface {
    /**
     * 获取当前布局文件的id
     *
     * @author hongfei.tao on 2016/8/6 14:03
     */
    int getLayoutId();

    /**
     * 初始化view
     *
     * @author hongfei.tao on 2016/8/6 14:03
     */
    void initView();

    /**
     * 初始化数据
     *
     * @author hongfei.tao on 2016/8/6 14:04
     */
    void initData();

}
