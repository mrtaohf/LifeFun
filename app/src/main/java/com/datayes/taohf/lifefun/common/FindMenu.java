package com.datayes.taohf.lifefun.common;

import com.datayes.taohf.lifefun.R;
import com.datayes.taohf.lifefun.activity.ConstellationActivity;
import com.datayes.taohf.lifefun.activity.JokeActivity;
import com.datayes.taohf.lifefun.activity.WeixinActivity;
import com.datayes.taohf.lifefun.base.BaseActivity;

/**
 * 发现页面
 * Created by hongfei.tao on 2016/8/14.
 */
public enum FindMenu {
    //微信精选，笑话大全，星座运势

    WEIXIN("微信精选", R.mipmap.weixin, WeixinActivity.class),
    XIAOHUA("笑话段子", R.mipmap.xiaohua, JokeActivity.class),
    XINGZUO("星座运势", R.mipmap.xingzuo, ConstellationActivity.class);

    private String title;
    private int icon;
    private Class<? extends BaseActivity> activity;

    public String getTitle() {
        return this.title;
    }

    public int getIcon() {
        return this.icon;
    }

    public Class<? extends BaseActivity> getActivity() {
        return this.activity;
    }


    FindMenu(String title, int icon, Class<? extends BaseActivity> activity) {
        this.title = title;
        this.icon = icon;
        this.activity = activity;
    }
}
